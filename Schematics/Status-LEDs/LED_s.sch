EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 60B516B1
P 4200 3900
F 0 "Q1" H 4390 3946 50  0000 L CNN
F 1 "2N2219" H 4390 3855 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 4400 3825 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 4200 3900 50  0001 L CNN
	1    4200 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R3
U 1 1 60B522CE
P 3550 3900
F 0 "R3" V 3345 3900 50  0000 C CNN
F 1 "2.4k" V 3436 3900 50  0000 C CNN
F 2 "" H 3550 3900 50  0001 C CNN
F 3 "~" H 3550 3900 50  0001 C CNN
	1    3550 3900
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 60B529A8
P 4300 3250
F 0 "R4" H 4368 3296 50  0000 L CNN
F 1 "430" H 4368 3205 50  0000 L CNN
F 2 "" H 4300 3250 50  0001 C CNN
F 3 "~" H 4300 3250 50  0001 C CNN
	1    4300 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 60B52FD7
P 6250 3550
F 0 "R1" V 6045 3550 50  0000 C CNN
F 1 "62" V 6136 3550 50  0000 C CNN
F 2 "" H 6250 3550 50  0001 C CNN
F 3 "~" H 6250 3550 50  0001 C CNN
	1    6250 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 60B5358E
P 6250 4000
F 0 "R2" V 6045 4000 50  0000 C CNN
F 1 "43" V 6136 4000 50  0000 C CNN
F 2 "" H 6250 4000 50  0001 C CNN
F 3 "~" H 6250 4000 50  0001 C CNN
	1    6250 4000
	0    1    1    0   
$EndComp
$Comp
L Device:LED D1
U 1 1 60B53D9F
P 5700 3550
F 0 "D1" H 5693 3766 50  0000 C CNN
F 1 "RED LED" H 5693 3675 50  0000 C CNN
F 2 "" H 5700 3550 50  0001 C CNN
F 3 "~" H 5700 3550 50  0001 C CNN
	1    5700 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60B54225
P 5700 4000
F 0 "D2" H 5693 4216 50  0000 C CNN
F 1 "GREEN LED" H 5693 4125 50  0000 C CNN
F 2 "" H 5700 4000 50  0001 C CNN
F 3 "~" H 5700 4000 50  0001 C CNN
	1    5700 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B55112
P 4300 4250
F 0 "#PWR?" H 4300 4000 50  0001 C CNN
F 1 "GND" H 4305 4077 50  0000 C CNN
F 2 "" H 4300 4250 50  0001 C CNN
F 3 "" H 4300 4250 50  0001 C CNN
	1    4300 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B553A4
P 5300 4250
F 0 "#PWR?" H 5300 4000 50  0001 C CNN
F 1 "GND" H 5305 4077 50  0000 C CNN
F 2 "" H 5300 4250 50  0001 C CNN
F 3 "" H 5300 4250 50  0001 C CNN
	1    5300 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3550 5300 3550
Wire Wire Line
	5300 3550 5300 4000
Wire Wire Line
	5550 4000 5300 4000
Connection ~ 5300 4000
Wire Wire Line
	5300 4000 5300 4250
Wire Wire Line
	6150 4000 5850 4000
Wire Wire Line
	6150 3550 5850 3550
Wire Wire Line
	6350 3550 6550 3550
Wire Wire Line
	6350 4000 6550 4000
Wire Wire Line
	4300 4100 4300 4250
Wire Wire Line
	4000 3900 3650 3900
Wire Wire Line
	3450 3900 3200 3900
Wire Wire Line
	4300 3700 4300 3650
Wire Wire Line
	4300 3150 4300 2950
Wire Wire Line
	4300 2950 4150 2950
$Comp
L Device:LED D3
U 1 1 60B5757E
P 4300 3500
F 0 "D3" V 4339 3383 50  0000 R CNN
F 1 "BLUE LED" V 4248 3383 50  0000 R CNN
F 2 "" H 4300 3500 50  0001 C CNN
F 3 "~" H 4300 3500 50  0001 C CNN
	1    4300 3500
	0    -1   -1   0   
$EndComp
Text GLabel 3200 3900 0    50   Input ~ 0
out1
Text GLabel 6550 3550 2    50   Input ~ 0
out2
Text GLabel 6550 4000 2    50   Input ~ 0
out3
$Comp
L power:+12V #PWR?
U 1 1 60B5882B
P 4150 2950
F 0 "#PWR?" H 4150 2800 50  0001 C CNN
F 1 "+12V" H 4165 3123 50  0000 C CNN
F 2 "" H 4150 2950 50  0001 C CNN
F 3 "" H 4150 2950 50  0001 C CNN
	1    4150 2950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
