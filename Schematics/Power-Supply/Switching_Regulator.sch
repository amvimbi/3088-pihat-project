EESchema Schematic File Version 4
LIBS:Switching Regulator-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C?
U 1 1 60BB0E2D
P 5950 3750
F 0 "C?" H 6065 3796 50  0000 L CNN
F 1 "C" H 6065 3705 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 5988 3600 50  0001 C CNN
F 3 "~" H 5950 3750 50  0001 C CNN
	1    5950 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D?
U 1 1 60BB0E33
P 4950 3750
F 0 "D?" H 4950 3966 50  0000 C CNN
F 1 "D_Zener" H 4950 3875 50  0000 C CNN
F 2 "Diode_THT:Diode_Bridge_15.1x15.1x6.3mm_P10.9mm" H 4950 3750 50  0001 C CNN
F 3 "~" H 4950 3750 50  0001 C CNN
	1    4950 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 2600 4950 2600
Wire Wire Line
	4950 3600 4950 2600
Connection ~ 4950 2600
Wire Wire Line
	4950 2600 5300 2600
Wire Wire Line
	5950 2600 5600 2600
Wire Wire Line
	5950 2600 5950 3600
Wire Wire Line
	5950 3850 5950 3900
Wire Wire Line
	5950 4700 4950 4700
Connection ~ 5950 3900
Wire Wire Line
	5950 3900 5950 4700
Wire Wire Line
	4950 3900 4950 4700
Wire Wire Line
	4150 2900 4150 3350
Wire Wire Line
	3950 2600 3450 2600
Wire Wire Line
	3450 2600 3450 3300
Connection ~ 4950 4700
Wire Wire Line
	4950 4700 3450 4700
Wire Wire Line
	3450 3550 3450 4700
Text GLabel 4150 3350 0    50   Input ~ 0
GPIO4
Text GLabel 3450 3300 0    50   Input ~ 0
Vin+
Text GLabel 3450 3550 0    50   Input ~ 0
GND
Wire Wire Line
	5950 2600 6950 2600
Wire Wire Line
	6950 2600 6950 3600
Connection ~ 5950 2600
Wire Wire Line
	5950 4700 6950 4700
Wire Wire Line
	6950 4700 6950 3900
Connection ~ 5950 4700
Text GLabel 6950 3900 0    50   Input ~ 0
GND
Text GLabel 6950 3600 0    50   Input ~ 0
Vout+
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 60BB0E55
P 4150 2700
F 0 "Q?" V 4493 2700 50  0000 C CNN
F 1 "Q_NMOS_DGS" V 4402 2700 50  0000 C CNN
F 2 "Package_DirectFET:DirectFET_MN" H 4350 2800 50  0001 C CNN
F 3 "~" H 4150 2700 50  0001 C CNN
	1    4150 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:L_Core_Ferrite L?
U 1 1 60BB0E5B
P 5450 2600
F 0 "L?" V 5675 2600 50  0000 C CNN
F 1 "L_Core_Ferrite" V 5584 2600 50  0000 C CNN
F 2 "Inductor_THT:Choke_EPCOS_B82722A" H 5450 2600 50  0001 C CNN
F 3 "~" H 5450 2600 50  0001 C CNN
	1    5450 2600
	0    -1   -1   0   
$EndComp
Text Notes 7350 7500 0    50   ~ 0
Switching Regulator
Text Notes 8100 7650 0    50   ~ 0
01/06/21
$EndSCHEMATC
